from decimal import Decimal
from django.db import models

from app.airlines.managers import AirplaneManager

PASSENGER_FUEL_CONSUMPTION = 0.002


class Airplane(models.Model):
    id = models.PositiveIntegerField(primary_key=True, unique=True)
    fuel_tank = models.PositiveIntegerField()
    fuel_consumption = models.DecimalField(max_digits=6, decimal_places=2)
    passengers = models.IntegerField(default=0)

    @property
    def total_fuel_consumption(self):
        return self.fuel_consumption + Decimal(self.passengers * PASSENGER_FUEL_CONSUMPTION)

    @property
    def max_flying_time(self):
        return self.fuel_tank / self.total_fuel_consumption

    objects = AirplaneManager()
