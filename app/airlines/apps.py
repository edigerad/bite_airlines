from django.apps import AppConfig


class AirlinesConfig(AppConfig):
    name = 'app.airlines'
