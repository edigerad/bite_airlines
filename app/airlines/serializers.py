from rest_framework import serializers
from app.airlines.models import Airplane


class AirplaneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Airplane
        fields = ('id', 'passengers', 'total_fuel_consumption', 'max_flying_time')

    def validate_id(self, value):
        if value > 10:
            raise serializers.ValidationError("Hey, the airplane doesn't exist")
        return value

# id, passengers
#
# is it enough to fly with the given amount of passengers to the destination with some fuel tank capacity?
#
# 1. calculate total fuel capacity/(fuel consumption per minute and by each passenger)
# 2.
