from django.contrib import admin
from app.airlines.models import Airplane


@admin.register(Airplane)
class AirplaneAdmin(admin.ModelAdmin):
    pass
