from django.db import models

from . import utils


class AirplaneManager(models.Manager):

    def get_queryset(self):
        return models.QuerySet(self.model, using=self._db)

    def create_instance(self, id, passengers):
        return self.model.objects.create(
            id=id,
            passengers=passengers,
            fuel_tank=utils.calculate_fuel_tank(id),
            fuel_consumption=utils.calculate_fuel_consumption(id),
        )

    def save_planes(self, data):
        """
        :param data: list of id and passengers numbers
        :return: QuerySet instances
        """
        # TODO: realize as described in documentation
        instances = [self.create_instance(item.get('id'), item.get('passengers', 0)) for item in data]
        return instances
