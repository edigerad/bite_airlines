from math import log
from decimal import Decimal

CAPACITY_VALUE = 200


def calculate_fuel_tank(pk):
    return pk * CAPACITY_VALUE


def calculate_fuel_consumption(id):
    return Decimal(log(id * 0.8))

