from django.test import TestCase

from app.airlines import utils
from app.airlines.models import Airplane


class TestAirplane(TestCase):

    def setUp(self):
        self.valid_data = {
            'id': 10,
            'passengers': 100,
        }

    def test_create_instance(self):
        instance = Airplane.objects.create_instance(**self.valid_data)
        self.assertEqual(instance.id, self.valid_data['id'])
        self.assertEqual(instance.fuel_tank, utils.calculate_fuel_tank(self.valid_data['id']))
        self.assertEqual(instance.fuel_consumption, utils.calculate_fuel_consumption(self.valid_data['id']))
