from django.apps import apps
from django.test import TestCase
from app.airlines.apps import AirlinesConfig


class AirlinesConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(AirlinesConfig.name, 'app.airlines')
        self.assertEqual(apps.get_app_config('airlines').name, 'app.airlines')
