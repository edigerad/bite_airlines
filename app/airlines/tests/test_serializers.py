from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase

from app.airlines.serializers import AirplaneSerializer


class TestAirplaneSerializer(APITestCase):

    def test_validate_id(self):
        serializer = AirplaneSerializer(data={'id': 4, 'passengers': 4})
        self.assertTrue(serializer.is_valid())

    def test_not_existed_id(self):
        serializer = AirplaneSerializer(data={'id': 12, 'passengers': 4})
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.data, {'id': 12, 'passengers': 4})
