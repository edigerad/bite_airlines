from django.urls import reverse
from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase, APIClient


class AirplaneViewSetTestCase(APITestCase):
    fixtures = ['airplanes.json', 'user.json']

    def setUp(self):
        login_url = reverse('rest_login')
        login_data = {'username': 'admin', 'password': 'admin'}

        login_response = self.client.post(login_url, data=login_data, format='json')
        self.token = login_response.data.get('token')
        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        self.url = reverse('airplanes-list')

    def test_calculate_fuel_consumptions_url(self):
        self.assertEqual(self.url, '/api/v0/airplanes/')

    def test_calculate_fuel_consumptions(self):
        payload = [{"id": 4, "passengers": 100},
                   {"id": 10, "passengers": 80},
                   {"id": 9, "passengers": 60}]
        response = self.api_client.post(self.url, data=payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_existed_planes(self):
        payload = [{"id": 1, "passengers": 100}]
        response = self.api_client.post(self.url, data=payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_wrong_token(self):
        self.token = 'wrong_token'
        self.api_client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        payload = [{"id": 10, "passengers": 100}]
        response = self.api_client.post(self.url, data=payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_total_fuel_consumptions(self):
        response = self.api_client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_not_existed_airplanes(self):
        payload = [{"id": 11, "passengers": 80}]
        response = self.api_client.post(self.url, data=payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, [{'id': [ErrorDetail(string="Hey, the airplane doesn't exist", code='invalid')]}])