from django.apps import apps
from django.test import TestCase
from app.users.apps import UsersConfig


class UsersConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(UsersConfig.name, 'app.users')
        self.assertEqual(apps.get_app_config('users').name, 'app.users')
