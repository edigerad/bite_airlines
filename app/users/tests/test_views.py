from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase, APIClient

from django.conf.urls import include, url


class TestUserDetailTestCase(APITestCase):
    fixtures = ['user.json']

    def setUp(self):
        self.url = reverse('rest_login')

    def test_login_url(self):
        self.assertEqual(self.url, '/api/v0/rest-auth/login/')

    def test_authentication_pass(self):
        payload = {'username': 'admin', 'password': 'admin'}
        response = self.client.post(self.url, data=payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data.get('token', None))

    def test_authentication_fail(self):
        payload = {'username': 'admin', 'password': '1234'}
        response = self.client.post(self.url, data=payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(response.data.get('token', None))
