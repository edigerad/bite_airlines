# Bite Airlines 

Service that helps to calculate airplanes' fuel tank of capacity and consumption.


##  Instructions for use and launch app.

1. Configure virtual environment settings

    1.1 Create virtual environment and activate it
    
    ```bash
    virtualenv -p python3.6 .venv
   source .venv/bin/activate
    ```
    1.2 Install required libraries

    ```bash
    pip install -r requirements.txt
    ```

2. Configure database administration 
    
    2.1. install postgresql
    
    2.2. open shell prompt
    ```bash
    sudo su - postgres
    psql
    ```
    2.3. Run the following commands:
    ```postgresplsql
    CREATE DATABASE bite_air;
    CREATE ROLE bite_air WITH PASSWORD 'bite_air';
    GRANT ALL PRIVILEGES ON DATABASE bite_air to bite_air;
    ALTER ROLE bite_air LOGIN CREATEDB;
    ```

3. Migrate the database and create super user

    ```bash
    ./manage.py migrate
    ```
   
    ```bash
    ./manage.py createsuperuser
   
   username:admin
   email:admin@bitesize.com
   password:admin
    ```

4. Create directory for logs

    ```bash
    mkdir logs
    ```

5. Run the server and check it by opening localhost:8000 in web browser

    ```bash
    ./manage.py runserver
    ```

 6. Import postman json files and test the Rest API via [Postman](https://www.getpostman.com/) app.